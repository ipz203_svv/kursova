﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ClassLabirith
{
    public class Maze
    {
        private enum Route
        {
            Right,
            Left,
            Down,
            Up
        }
        private class Cell
        {
            public int y;
            public int x;

            public Cell(int y, int x) 
            {
                this.y = y;
                this.x = x;
            }

        }

       

        private List<Route> DiggerRoute = new List<Route>();

        private int [][] Map { get; set; } 
        private int AllStride { get; set; }
        private int stride { get; set; } 
        private Random RandGenerete = new Random();
        private List<Cell> Way = new List<Cell>(); 
        public Maze(int rows, int columns) 
        {
            Map = new int[rows][];
            AllStride = (rows - 1)* (columns-1) / 4; 

            for (int y = 0; y < rows;y++)
            {
                Map[y] = new int[columns];
                for(int x=0;x<columns;x++)
                {
                    Map[y][x] = 1;
                }
            }

            Map[1][1] = 0;
            Map[1][0] = 0;
            stride = 1;
            Way.Add(new Cell(1,1));

        }
            public Bitmap drawingMap(int width, int height, Color wall, Color free, int size)
        {
            int coordX;
            int coordY;
            Color color;
            Bitmap image = new Bitmap(width, height);
            Graphics context = Graphics.FromImage(image);
           
            int left = width / 2 - (size * Map[0].Length + (Map[0].Length-1))/2;
            int up = height / 2 - (size * Map.Length + (Map.Length  -1 ))/2;
            
            for (int y = 0; y < Map.Length; y++)
            {
                for (int x =0; x<Map[0].Length; x++)
                {
                    coordX = x * size + 1 * x + left;
                    coordY = y * size + 1 * y + up;
                    if (Map[y][x] == 1)
                    {
                        color = wall;
                    }
                    else
                    {
                        color = free;
                    }
                    context.FillRectangle(new SolidBrush(color), coordX, coordY, size, size);
                }
            }

            context.FillRectangle(new SolidBrush(Color.LightGray), Way.Last().x * size + Way.Last().x + left, Way.Last().y * size + Way.Last().y + up, size, size);
            return image;
        }
        
        private void Stride() 
        {
            DiggerRoute.Clear();
            Cell last = Way.Last();
            if (last.x + 2 < Map[0].Length && Map[last.y][last.x + 2] == 1)
            {
                DiggerRoute.Add(Route.Right);
            }
            if (last.x - 2 > 0 && Map[last.y][last.x - 2] == 1) 
            {
                DiggerRoute.Add(Route.Left);
            }
            if (last.y + 2 < Map.Length && Map[last.y + 2][last.x] == 1) 
            {
                DiggerRoute.Add(Route.Down);
            }
            if (last.y - 2 > 0 && Map[last.y - 2][last.x] == 1) 
            {
                DiggerRoute.Add(Route.Up); 
            }

            if (DiggerRoute.Count > 0)
            {
                stride++;
                int rnd = RandGenerete.Next(DiggerRoute.Count);
                switch (DiggerRoute[rnd])
                {
                    case Route.Right: 
                        Map[last.y][last.x + 1] = 0; 
                        Map[last.y][last.x + 2] = 0; 
                        Way.Add(new Cell(last.y, last.x + 2)); 
                        break;
                    case Route.Left:
                        Map[last.y][last.x - 1] = 0; 
                        Map[last.y][last.x - 2] = 0; 
                        Way.Add(new Cell(last.y, last.x - 2)); 
                        break;
                    case Route.Down:
                        Map[last.y + 1][last.x] = 0;
                        Map[last.y + 2][last.x] = 0;
                        Way.Add(new Cell(last.y + 2, last.x));
                        break;
                    case Route.Up:
                        Map[last.y - 1][last.x] = 0;
                        Map[last.y - 2][last.x] = 0;
                        Way.Add(new Cell(last.y - 2, last.x));
                        break;
                }
            }
            else
            {
                Way.RemoveAt(Way.Count - 1);
            }
            
        }

        public void build() 
        {
            while (stride < AllStride)
            {
                Stride();
            }
            
            DiggerRoute.Clear(); 
            DiggerRoute.Add(Route.Right);  
            DiggerRoute.Add(Route.Left);     
            for (int i = 1; i < 2; i++)
            {
                int rndm = RandGenerete.Next(0, DiggerRoute.Count); 
                DiggerRoute.RemoveAt(rndm);
            }
            for (int i = 0; i < 1; i++)
            {
                int rndm; 
                Route wall = DiggerRoute[i];
                switch (wall)
                {
                    case Route.Left:
                        rndm = RandGenerete.Next(1, Map.Length - 1);
                        if (rndm % 2 == 0)
                        {
                            rndm++;
                        }
                        Map[rndm][0] = 0;
                        break;
                    case Route.Right:
                        rndm= RandGenerete.Next(1, Map.Length - 1);
                        if (rndm % 2 == 0)
                        {
                            rndm++;
                        }
                        Map[rndm][Map[0].Length-1] = 0;
                        break;
                    
                }
            }

        }
    }
}
