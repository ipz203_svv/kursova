﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLabirith;


namespace Labirith
{
    public partial class Form1 : Form
    {

        private object currObject = null;
        int i = 0;

        public Form1()
        {
            InitializeComponent();
            this.MouseMove += new MouseEventHandler(MouseEvent);
            this.MouseClick += new MouseEventHandler(mouseClick);
        }

        private void mouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button.ToString() == "Right")
                currObject = null;
        }
        private void MouseEvent(object sender, MouseEventArgs e)
        {

            if (currObject != null)
            {
                player.Location = this.PointToClient(Control.MousePosition);
            }
        }
        private void player_Click(object sender, EventArgs e)
        {
            currObject = sender;
            timer1.Enabled = true;

        }
        private void buttonGen_Click(object sender, EventArgs e)
        {
            int x, y;
            x = (int)numericUpDownX.Value;
            y = (int)numericUpDownY.Value;
            Maze maze = new Maze(x, y);
            maze.build();  
            Form1.ActiveForm.BackgroundImage = maze.drawingMap(Form1.ActiveForm.Width+360, Form1.ActiveForm.Height, Color.SaddleBrown, Color.LightGray, 18);
            

        }
        
        private void pictureBox2_MouseEnter(object sender, EventArgs e)
        {
            if (i > 5)
            {
                MessageBox.Show("You Won!\n" + "Час за який лабіринт пройдено - " + i + " секунд");
                timer1.Enabled = false;
                this.Close();
            }
        }

        private void pictureBox3_MouseEnter(object sender, EventArgs e)
        {
            if(i > 5)
            {
            MessageBox.Show("You Won!\n"+"Час за який лабіринт пройдено - " + i + " секунд");
            timer1.Enabled = false;
            this.Close();
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            i++;
            labelTime.Text = i.ToString();
        }
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
           System.Diagnostics.Process.Start("https://learn.ztu.edu.ua/user/profile.php?id=7995");
        }
        private void Help_Click(object sender, EventArgs e)
        {
            label6.Visible = true;
            label7.Visible = true;
        }
        private void Form1_Load_1(object sender, EventArgs e)
        {
        }

        private void Form1_MouseClick_1(object sender, MouseEventArgs e)
        {   
        }
        private void button1_Click(object sender, EventArgs e)
        { }
        private void Form1_Load(object sender, EventArgs e)
        { }

        private void numericUpDownX_ValueChanged(object sender, EventArgs e)
        {

        }

        private void numericUpDownY_ValueChanged(object sender, EventArgs e)
        {

        }

        private void labelXY_Click(object sender, EventArgs e)
        {

        }

        private void Form1_MouseEnter(object sender, EventArgs e)
        {
            
        }

        

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }
    }
}
